import React, {Component} from 'react';
import './Post.css'

const Post = ({title, content}) => {
    return (
      <div className="postContainer">
        <div className="postTitle">{title}</div>
        <div>{content}</div>
      </div>
    );
};

export default Post;