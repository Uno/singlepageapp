import React, {Component} from 'react';
import Waypoint from 'react-waypoint';

import Post from './../post/Post';
import Templates from './templates';
import Navbar from "../navbar/Navbar";
import './PostList.css'

class PostList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      first: {name: 'First', link: '#first', active: false},
      second: {name: 'Second', link: '#second', active: false},
      third: {name: 'Third', link: '#third', active: false}
    };

    this.updateState = this.updateState.bind(this);
  }

  renderContent(tmpl) {
    return tmpl.map(p => <div className="sectionDiv">{p}</div>)
  }

  updateState(name) {
    const copy = this.state;
    Object.keys(copy).map((key) => copy[key].active = false);
    copy[name].active = true;
    this.setState(copy);
  }

  render() {
    return (
      <div>
        <div>
          <Navbar
            navList={this.state}
            menuClick={(name) => this.updateState(name)}
          />
        </div>
        <div className="postContainer">
          <div id="section1">
            <a className="anchor" id="first" />
            <Waypoint
              onEnter={() => this.updateState('first')}
              bottomOffset="50%"
            />
            <Post
              title="Section 1"
              content={this.renderContent(Templates.section1)}
            />
          </div>

          <div className="separator" />

          <div id="section2">
            <a className="anchor" id="second" />
            <Waypoint
              onEnter={() => this.updateState('second')}
              bottomOffset="50%"
            />
            <Post
              id="section2"
              title="Section 2"
              content={this.renderContent(Templates.section2)}
            />
          </div>

          <div className="separator" />

          <div id="section3">
            <a className="anchor" id="third" />
            <Waypoint
              onEnter={() => this.updateState('third')}
              bottomOffset="50%"
            />
            <Post
              id="section3"
              title="Section 3"
              content={this.renderContent(Templates.section3)}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default PostList;