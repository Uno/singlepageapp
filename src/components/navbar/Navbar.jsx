import React, {Component} from 'react';
import './Navbar.css';

class Navbar extends Component {
  renderListItem(e) {
    return (
      <li onClick={() => this.props.menuClick(e.name.toLowerCase())}>
        <a href={e.link} className={e.active ? 'active' : null}>{e.name}</a>
      </li>
    );
  }

  render() {
    return (
      <div>
        <ul>
          {Object.keys(this.props.navList).map((key) => this.renderListItem(this.props.navList[key]))}
        </ul>
      </div>
    );
  }
}


export default Navbar;